FROM python:3.12.6-alpine3.20

WORKDIR /code

COPY ./requirements.* ./

RUN apk add --virtual build-deps build-base python3-dev && \
    pip install --no-cache-dir --upgrade --compile -r /code/requirements.txt && \
    apk del build-deps && \
    rm -rf /var/cache/apk/*

COPY ./app /code/app

EXPOSE 80

ENTRYPOINT ["uvicorn", "--host", "0.0.0.0", "--port", "80", "app.main:app"]
