# Bootstrap OTP and OIDC Auth

This webservice generates a secret that can validated with a one time password
(OTP) and OpenID Connect token to grant access for installing a machine via YAC
backend.

## Minimal Example

    docker run --rm --name bootstrap-auth -p 8080:80 manumeter7/bootstrap-auth:latest

You should be able to access the API and the documentation at:
http://localhost:8080/docs

### Images/Tags

The container images are available with the following tags at:
https://hub.docker.com/r/manumeter7/bootstrap-auth

  - *latest*: The latest stable release
  - *v1*, *v2*, ...: The specific stable release (including minor updates,
    exluding release candidates)
  - *v1.0*, *v2.1*, *v3.0rc2*, ...: The specific minor version (will never be
    updated)
  - *testing*: Every latest commit/tag built before testing

## Development

### Upgrade Environment

- Check on https://hub.docker.com/r/alpine/helm and
  https://hub.docker.com/_/redis for new versions and adjust the tags in
  `.gitlab-ci.yml`.

- Check on https://hub.docker.com/_/python for new versions and adjust the tag
  in the `FROM` instruction of `./Dockerfile`. (Use a most specific tag to allow
  reproducable builds.)

- Build container and generate a new requirements file with:

      docker run --rm -v "$(pwd)/requirements.in:/r.in:ro" --entrypoint sh bootstrap-auth:latest -c \
          "pip install pip-tools &>/dev/null; pip-compile -o - /r.in" > ./requirements.txt

      docker build -t bootstrap-auth .
