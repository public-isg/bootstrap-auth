import logging
from typing import Literal

from pydantic_settings import BaseSettings, SettingsConfigDict

# pylint: disable=global-statement,global-variable-not-assigned
__ENV = None
__LOG = None

__LOGLEVELS = {
    'critical': logging.CRITICAL,
    'error': logging.ERROR,
    'warning': logging.WARNING,
    'info': logging.INFO,
    'debug': logging.DEBUG,
}

class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_prefix='bootstrap_')

    root_path: str = '/'
    log_level: Literal['critical', 'error', 'warning', 'info', 'debug'] = 'info'

    oidc_url: str = 'https://localhost/.well-known/openid-configuration'
    oidc_client_ids: str = '' # space separated list of accepted client_ids (first one will be used in Swagger UI)
    oidc_jwt_name: str = '{name}' # available format vars: all values from the JWT ID-Token
    oidc_jwt_email: str = '{mail}' # available format vars: all values from the JWT ID-Token
    oidc_jwt_full_name: str = '{givenName} {surname}' # available format vars: all values from the JWT ID-Token

    otp_token_expire: int = 6 # in hours
    otp_secret2_length: int = 15

    redis_url: str = 'redis://localhost:6379'

    yac_url: str = 'https://localhost/yac'
    yac_secret: str = ''
    yac_type: str = 'host'
    yac_name: str = '{secret2}.localhost' # available format vars: token, user, secret2
    yac_yaml: str = 'user: {user}' # available format vars: token, user, secret2

def env(var: str, default = None):
    global __ENV
    if __ENV is None:
        __ENV = Settings()
    return getattr(__ENV, var, default)

def log(level: str, message: str):
    global __LOG
    if __LOG is None:
        __LOG = logging.getLogger()
        __LOG.setLevel(__LOGLEVELS.get(env('log_level')))
    __LOG.log(__LOGLEVELS.get(level, logging.INFO), message)
