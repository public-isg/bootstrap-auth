from fastapi import HTTPException
from fastapi import Request
from fastapi import Depends
from fastapi import status
from fastapi.security.open_id_connect_url import OpenIdConnect
from authlib.integrations.starlette_client import OAuth
from pydantic import BaseModel
from pydantic import Field
from typing_extensions import Annotated

from app.config import env, log

authlib_oauth = OAuth()
authlib_oauth.register(
    name = 'oidc',
    server_metadata_url = env('oidc_url'),
    client_kwargs = {'scope': 'openid'},
)

fastapi_oauth2 = OpenIdConnect(
    openIdConnectUrl = env('oidc_url'),
    scheme_name = 'OpenID Connect',
)

class User(BaseModel):
    name: Annotated[str, Field(example='user73')]
    email: Annotated[str, Field(example='user73@example.com')]
    full_name: Annotated[str, Field(example='John Doe')]

async def __parse_token(token) -> dict:
    try:
        user = await authlib_oauth.oidc.parse_id_token(
            token = {'id_token': token[7:] if token[:7] == 'Bearer ' else token},
            nonce = None, # can be ignored because we're using PKCE
        )
        if user['aud'] not in env('oidc_client_ids').split(' '):
            raise Exception(f'"{user["aud"]}" is not an accepted client_id')
    except Exception as exp:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = f'Supplied authentication could not be validated ({exp})',
        )
    return user

async def current_user(token: Annotated[str, Depends(fastapi_oauth2)]) -> User:
    user = await __parse_token(token)
    return User(
        name = env('oidc_jwt_name').format(**user),
        email = env('oidc_jwt_email').format(**user),
        full_name = env('oidc_jwt_full_name').format(**user),
    )

async def current_token(token: Annotated[str, Depends(fastapi_oauth2)]) -> str:
    _ = __parse_token(token) # still want to validate the token before returning it
    return token[7:] if token[:7] == 'Bearer ' else token
