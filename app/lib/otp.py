import json
from datetime import datetime
from typing_extensions import Annotated

import redis
from pydantic import BaseModel, Field

from app.config import env

cache = redis.from_url(env('redis_url'), decode_responses=True)

class AuthRequest(BaseModel):
    token: Annotated[str, Field(
        example = '123456',
    )]
    secret1: Annotated[str, Field(
        example = '4pw89weo3r30er6z4pw89weo3r30er6',
    )]
    expire: Annotated[str, Field(
        example = '2024-06-17T10:15:49.432941',
        description = 'Expiry date in UTC, ISO8601 format',
    )]

class ValidAuth(BaseModel):
    token: Annotated[str, Field(
        example='123456'
    )]
    user: Annotated[str, Field(
        example='myaccount'
    )]
    secret2: Annotated[str, Field(
        example='4pw89weo3r30er6'
    )]

def cleanup() -> None:
    for key in cache.scan_iter('auth:*'):
        expire = json.loads(cache.get(key)).get('expire')
        if datetime.utcnow() >= datetime.fromisoformat(expire):
            cache.delete(key)

def add(auth: AuthRequest) -> bool:
    cleanup()
    if has(auth.token):
        return False
    cache.set(f'auth:{auth.token}', json.dumps({
        'valid': False,
        'user': None,
        'secret1': auth.secret1,
        'secret2': None,
        'expire': auth.expire,
    }))
    return True

def has(token: str) -> bool:
    cleanup()
    return cache.exists(f'auth:{token}')

def is_valid(token: str) -> bool:
    if not has(token):
        return False
    return json.loads(cache.get(f'auth:{token}')).get('valid', False)

def is_correct(token: str, secret1: str) -> bool:
    if not has(token):
        return False
    return secret1 == json.loads(cache.get(f'auth:{token}')).get('secret1', None)

def validate(auth: ValidAuth) -> None:
    before = json.loads(cache.get(f'auth:{auth.token}'))
    cache.set(f'auth:{auth.token}', json.dumps({
        'valid': True,
        'user': auth.user,
        'secret1': before.get('secret1'),
        'secret2': auth.secret2,
        'expire': before.get('expire'),
    }))

def get(token: str) -> ValidAuth:
    before = json.loads(cache.get(f'auth:{token}'))
    return ValidAuth(
        token = token,
        user = before.get('user'),
        secret2 = before.get('secret2'),
    )
