from urllib.parse import quote_plus
import httpx

from app.lib import otp
from app.config import env

def authorize(auth: otp.ValidAuth, token: str) -> bool:
    msg = f'Created through Authenticator'
    response = httpx.post(
        f'{env("yac_url")}/entity/{env("yac_type")}?msg={quote_plus(msg)}',
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {token}',
            'YAC-Auth-Secret': env('yac_secret'),
        },
        json = {
            'name': env('yac_name').format(**vars(auth)),
            'yaml': '---\n{}\n'.format(env('yac_yaml').format(**vars(auth))),
        }
    )

    if response.status_code not in [201, 409]:
        raise ReferenceError(response.text)
    return response.status_code == 201
