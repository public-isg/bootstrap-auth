from urllib.parse import urlparse

from fastapi import FastAPI
from fastapi.openapi.utils import get_openapi

from app.config import env
from app.router import status
from app.router import otp

TITLE = 'Bootstrap OTP and OIDC Auth'
VERSION = '0.1'
DESCRIPTION = f'''

## Authentication

We are using the OpenID Connect provider
[{urlparse(env("oidc_url")).netloc}]({env("oidc_url")})
for authentication. You need to send a valid `id_token` via `Authorization: Bearer`
header to all API endpoints that require authentication.

Only the following `client_id`s are accepted:
`{'` `'.join(env("oidc_client_ids").split(' '))}`

For interactive user logins, `authentication_code with PKCE` flow (with a
dummy `nonce` parameter that won't be validated) is recommended. For
automated login in scripts/software, use the `password` flow instead (therefore
you will also need the `client_secret`).

## Source and Issues

Repository on [GitLab](https://gitlab.inf.ethz.ch/public-isg/bootstrap-auth)
'''
CONTACT = {
    'name': 'Manuel (isginf)',
    'email': 'manuel.maestinger@inf.ethz.ch',
}
LICENSE = {
    'name': 'GNU GPLv3',
    'url': 'https://www.gnu.org/licenses/gpl-3.0.html',
}

app = FastAPI(
    title = TITLE,
    description = DESCRIPTION,
    version = VERSION,
    root_path = '' if env('root_path') == '/' else env('root_path'),
    contact = CONTACT,
    license_info = LICENSE,
    docs_url = '/docs',
    redoc_url = None,
    swagger_ui_parameters = {
        'displayRequestDuration': True,
        'persistAuthorization': True
    },
    swagger_ui_init_oauth = {
        'scopes': 'openid',
        'clientId': env('oidc_client_ids').split(' ')[0],
        'usePkceWithAuthorizationCodeGrant': True,
        'additionalQueryStringParams': {'nonce': 0},
    },
)

app.include_router(status.router, tags=['Status'])
app.include_router(otp.router, tags=['One Time Password'])

# Hack from: https://github.com/fastapi/fastapi/discussions/8557
def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title = TITLE,
        description = DESCRIPTION,
        version = VERSION,
        contact = CONTACT,
        license_info = LICENSE,
        routes = app.routes,
    )
    openapi_schema['components']['securitySchemes']['OpenID Connect']['x-tokenName'] = 'id_token'
    app.openapi_schema = openapi_schema
    return app.openapi_schema
app.openapi = custom_openapi
# Hack end
