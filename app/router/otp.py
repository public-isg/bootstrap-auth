import random
import string
from datetime import datetime, timedelta
from typing_extensions import Annotated

from fastapi import APIRouter, Depends, HTTPException, status, Query, Path

from app.config import env
from app.lib import oidc
from app.lib import otp
from app.lib import yac

router = APIRouter()

@router.post(
    '/otp',
    summary = 'Create an authentication request',
    status_code = status.HTTP_201_CREATED,
    responses = {
        500: {'content': {'text/plain': {'schema': {'type': 'string'}}}},
    },
)
def create_otp() -> otp.AuthRequest:
    secret1 = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(31))

    limit = 100
    while limit > 0:
        limit = limit - 1
        auth = otp.AuthRequest(
            token = f'{random.randrange(0, 999999):06d}',
            secret1 = secret1,
            expire = (datetime.utcnow() + timedelta(hours=env('otp_token_expire'))).isoformat(),
        )
        if otp.add(auth):
            break

    if limit == 0:
        raise OverflowError('Unable to find random token')

    return auth

@router.put(
    '/otp/{token}',
    summary = 'Validate an authentication request',
    status_code = status.HTTP_204_NO_CONTENT,
    responses = {
        401: {'content': {'application/json': {'schema':
                {'type': 'object', 'properties': {'detail': {'type': 'string'}}}
             }}},
        403: {'content': {'application/json': {'schema':
                {'type': 'object', 'properties': {'detail': {'type': 'string'}}}
             }}},
        404: {'content': {'application/json': {'schema':
                {'type': 'object', 'properties': {'detail': {'type': 'string'}}}
             }}},
        500: {'content': {'text/plain': {'schema': {'type': 'string'}}}},
    },
)
def validate_otp(
    token: Annotated[str, Path(pattern=r'^[0-9]{6}$')],
    user: Annotated[oidc.User, Depends(oidc.current_user)],
    user_token: Annotated[str, Depends(oidc.current_token)]
) -> None:
    if not otp.has(token):
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND,
            detail = 'This token is expired or it does not exist',
        )

    if otp.is_valid(token):
        raise HTTPException(
            status_code = status.HTTP_403_FORBIDDEN,
            detail = 'This token is already validated',
        )

    limit = 20
    while limit > 0:
        limit = limit - 1
        secret2 = ''.join(
            random.choice(string.ascii_lowercase + string.digits)
            for _ in range(env('otp_secret2_length'))
        )
        auth = otp.ValidAuth(
            token = token,
            user = user.name,
            secret2 = secret2,
        )
        if yac.authorize(auth, user_token):
            break

    if limit == 0:
        raise OverflowError('Unable to find random secret2')

    otp.validate(auth)

@router.get(
    '/otp/{token}',
    summary = 'Get validated authentication request',
    responses = {
        404: {'content': {'application/json': {'schema':
                {'type': 'object', 'properties': {'detail': {'type': 'string'}}}
             }}},
        425: {'content': {'application/json': {'schema':
                {'type': 'object', 'properties': {'detail': {'type': 'string'}}}
             }}},
        500: {'content': {'text/plain': {'schema': {'type': 'string'}}}},
    },
)
def get_otp_secret(
    token: Annotated[str, Path(pattern=r'^[0-9]{6}$')],
    secret1: Annotated[str, Query(pattern=r'^[a-z0-9]{31}$')]
) -> otp.ValidAuth:
    if not otp.is_correct(token, secret1):
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND,
            detail = 'This token is expired, does not exist or secret1 is wrong',
        )

    if not otp.is_valid(token):
        raise HTTPException(
            status_code = status.HTTP_425_TOO_EARLY,
            detail = 'This token has not been validated yet',
        )

    return otp.get(token)
