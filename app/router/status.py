from typing_extensions import Annotated

from fastapi import APIRouter
from fastapi import status
from fastapi import Depends

from app.lib import oidc

router = APIRouter()

@router.get(
    '/health',
    summary = 'Test if the application is running',
    status_code = status.HTTP_204_NO_CONTENT,
    responses = {
        500: {'content': {'text/plain': {'schema': {'type': 'string'}}}},
    },
)
def get_health() -> None:
    '''
    Will check if the API is working.
    '''

@router.get(
    '/me',
    summary = 'Test the token for validity',
    responses = {
        401: {'content': {'application/json': {'schema':
                {'type': 'object', 'properties': {'detail': {'type': 'string'}}}
             }}},
        403: {'content': {'application/json': {'schema':
                {'type': 'object', 'properties': {'detail': {'type': 'string'}}}
             }}},
        500: {'content': {'text/plain': {'schema': {'type': 'string'}}}},
    },
)
def me(user: Annotated[oidc.User, Depends(oidc.current_user)]) -> oidc.User:
    '''
    Will validate the OpenID Connect ID Token and return some user data.
    '''
    return user
